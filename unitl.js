let btnStart = document.getElementById('btnClick');
let startTime;
let stopTime;


function date() {
    let today = new Date();
    let day = today.getDate();
    let month = today.getMonth();
    let year = today.getFullYear();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    document.getElementById('txt').innerHTML ="Date: "+ day + "/" + month + "/" + year + ", " + h + ":" + m + ":" + s;
    var t = setInterval(date, 1000);
}


btnStart.onclick = () =>{
    if(btnStart.textContent === "Start"){
        document.getElementById('btnClick').innerHTML = "Stop";
        document.getElementById('btnClick').style = "background-color: red; color:white;"
        timeStart();
    }else if(btnStart.textContent === "Stop"){
        timeStop();
        var t = Math.ceil(timeCalculate());
        document.getElementById('btnClick').innerHTML = "Clear";
        document.getElementById('btnClick').style = "background-color: gold;color:white; "
        document.getElementById('min').innerHTML = ""+ t + " Minute";
        document.getElementById('riels').innerHTML = ""+payment()+" Riels";
        
    }else if(btnStart.textContent == "Clear"){
        document.getElementById('btnClick').innerHTML = "Start";
        document.getElementById('btnClick').style = "background-color: green; color:white;"
        timeClear();
    }
}

function timeStart(){
    let today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    startTime = (h*3600) + (m*60) + s;
    var start = today.toLocaleString('en-US', { hour: 'numeric',minute: 'numeric',second:'numeric', hour12: true })
    document.getElementById('start').innerHTML = "Start at: "+start;
}
function timeStop(){
    let today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    stopTime = (h*3600) + (m*60) + s;
    var stop = today.toLocaleString('en-US', { hour: 'numeric',minute: 'numeric',second:'numeric', hour12: true })
    document.getElementById('stop').innerHTML = "Stop at: " + stop;
}

function timeClear(){
    document.getElementById('start').innerHTML = "start at: 00:00:00";
    document.getElementById('stop').innerHTML = "Stop at: 00:00:00";
    document.getElementById('min').innerHTML = "0 Minute(s) ";
    document.getElementById('riels').innerHTML = "0 Riels(s) ";
}
function timeCalculate(){
    return (stopTime - startTime)/60;
}
function payment(){
    var times = timeCalculate();
    var money = 0;
    if(times>0 && times <15 ){
        money = 500;
    }
    else if(times>16 && times <30 ){
        money = 1000;
    }
    else if(times>31 && times <60 ){
        money = 1500;
    }
    else if(times>=60){
        money = timeCalculate * 30.76;
    }
    return money;
}